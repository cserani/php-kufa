<section class="testimonial-area primary-bg pt-115 pb-115">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-6 col-lg-8">
                <div class="section-title text-center mb-70">
                    <span>testimonial</span>
                    <h2>happy customer quotes</h2>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-10">
                <div class="testimonial-active">
                <?php
                
                foreach($testimonialQuery as $value) {

                
                ?>
                    <div class="single-testimonial text-center">
                        <div class="testi-avatar">
                            <img src="backend/testimonial/images/<?=$value['image']?>" alt="img" style="width:100px">
                        </div>
                        <div class="testi-content">
                            <h4><span>“</span><?=$value['description']?> <span>”</span></h4>
                            <div class="testi-avatar-info">
                                <h5><?=$value['name']?> </h5>
                                <span><?=$value['designation']?> </span>
                            </div>
                        </div>
                    </div>

                <?php }?>
                </div>
            </div>
        </div>
    </div>
</section>