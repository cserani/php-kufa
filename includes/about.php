
<section id="about" class="about-area primary-bg pt-120 pb-120">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="about-img">
                    <img src="backend/about/images/<?= $aboutqueryresult['picture'] ?>" title="me-01" alt="me-01" style="width: 60%; height: 60%;">
                </div>
            </div>
            <div class="col-lg-6 pr-90">
                <div class="section-title mb-25">
                    <span>Introduction</span>
                    <h2>About Me</h2>
                </div>
                <div class="about-content">
                    <p></p><?= $aboutqueryresult['about_me'] ?></p>
                    <h3>Education:</h3>
                </div>
                <!-- Education Item -->
                <?php
                foreach($bannerQuery as $key=>$value) {
                    ?>
                    <div class="education">
                        <div class="year"><?=$value['year']?></div>
                        <div class="line"></div>
                        <div class="location">
                            <span><?=$value['title']?></span>
                            <div class="progressWrapper">
                                <div class="progress">
                                    <div class="progress-bar wow slideInLefts" data-wow-delay="0.2s"
                                         data-wow-duration="2s" role="progressbar" style="width: <?=$value['progress']?>%;"
                                         aria-valuenow="<?=$value['progress']?>" aria-valuemin="0" aria-valuemax="100"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <!-- End Education Item -->
            </div>
        </div>
    </div>
</section>