<div class="barnd-area pt-100 pb-100">
    <div class="container">
        <div class="row brand-active">
        <?php 
        foreach ($brandQuery as $value) {
        ?>
            <div class="col-xl-2">
                <div class="single-brand">
                    <img src="backend/brand/images/<?=$value['image']?>" alt="img">
                </div>
            </div>
            <?php   
            }
            ?>
        
        </div>
    </div>
</div>