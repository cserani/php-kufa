<div class="extra-info">
    <div class="close-icon menu-close">
        <button>
            <i class="far fa-window-close"></i>
        </button>
    </div>
    <div class="logo-side mb-30">
        <a href="index-2.html">
            <img src="asset/img/logo/logo.png" alt="" />
        </a>
    </div>
    <div class="side-info mb-30">
        <div class="contact-list mb-30">
            <h4>Office Address</h4>
            <p>123/A, Miranda City Likaoli
                Prikano, Dope</p>
        </div>
        <div class="contact-list mb-30">
            <h4>Phone Number</h4>
            <p>+0989 7876 9865 9</p>
        </div>
        <div class="contact-list mb-30">
            <h4>Email Address</h4>
            <p>info@example.com</p>
        </div>
    </div>
    <div class="social-icon-right mt-20">
        <a href="#"><i class="fab fa-facebook-f"></i></a>
        <a href="#"><i class="fab fa-twitter"></i></a>
        <a href="#"><i class="fab fa-google-plus-g"></i></a>
        <a href="#"><i class="fab fa-instagram"></i></a>
    </div>
</div>
<div class="offcanvas-overly"></div>