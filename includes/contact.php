<section id="contact" class="contact-area primary-bg pt-120 pb-120">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6">
                <div class="section-title mb-20">
                    <span>information</span>
                    <h2><?=$contactresult['title']?></h2>
                </div>
                <div class="contact-content">
                    <p><?=$contactresult['description']?></p>
                    <h5>OFFICE IN <span>Bangladesh</span></h5>
                    <div class="contact-list">
                        <ul>
                            <li><i class="fas fa-map-marker"></i><span>Address :</span><?=$contactresult['address']?></li>
                            <li><i class="fas fa-headphones"></i><span>Phone :</span><?=$contactresult['mobile']?></li>
                            <li><i class="fas fa-globe-asia"></i><span>e-mail :</span><?=$contactresult['email']?></li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="contact-form">
                    <form action="includes/contact-post.php" method="post">
                        <input name="name" type="text" placeholder="your name *">
                        <input name="email" type="email" placeholder="your email *">
                        <textarea name="message" id="message" placeholder="your message *"></textarea>
                        <button type="submit" class="btn">SEND</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>