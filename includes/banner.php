<section id="home" class="banner-area banner-bg fix">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-xl-7 col-lg-6">
                <div class="banner-content">
                    <h6 class="wow fadeInUp" data-wow-delay="0.2s">HELLO!</h6>
                    <h2 class="wow fadeInUp" data-wow-delay="0.4s"><?=$bannerresult['title']?></h2>
                    <p class="wow fadeInUp" data-wow-delay="0.6s"><?=$bannerresult['description']?></p>
                    <div class="banner-social wow fadeInUp" data-wow-delay="0.8s">
                        <ul>
                            <li><a href="<?=$bannerresult['facebook']?>"><i class="fab fa-facebook-f"></i></a></li>
                            <li><a href="<?=$bannerresult['twitter']?>"><i class="fab fa-twitter"></i></a></li>
                            <li><a href="<?=$bannerresult['linkedin']?>"><i class="fab fa-linkedin"></i></a></li>
                            <li><a href="<?=$bannerresult['pinterest']?>"><i class="fab fa-pinterest"></i></a></li>
                        </ul>
                    </div>
                    <a href="#" class="btn wow fadeInUp" data-wow-delay="1s">SEE PORTFOLIOS</a>
                </div>
            </div>
            <div class="col-xl-5 col-lg-6 d-none d-lg-block">
                <div class="banner-img text-right">
                    <img src="backend/banner/images/<?=$bannerresult['image']?>" alt="" style="width: 100%; height: 70%">
                </div>
            </div>
        </div>
    </div>
    <div class="banner-shape"><img src="asset/img/shape/dot_circle.png" class="rotateme" alt="img"></div>
</section>