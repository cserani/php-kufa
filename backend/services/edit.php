<?php
require '../database/connect.php';

$id = $_GET['id'];
$sql= "SELECT * FROM `services` WHERE `id`='$id'";
$query= mysqli_query($database,$sql);
$result= mysqli_fetch_assoc($query);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php';?>
</head>
<body>
<?php include '../layouts/navbar.php';?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <form action="update.php" method="post" enctype="multipart/form-data">
                    <input type="text" value="<?=$id?>" name="id" hidden>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title"
                               aria-describedby="emailHelp" placeholder="Enter your title"  name="title" value="<?= $result['title']?>">
                    </div>
                    <div class="form-group">
                        <label for="year">Description</label>
                        <input type="text" id="des"
                               placeholder="Enter Des."  class="form-control" name="des"  value="<?= $result['des']?>">
                    </div>
                    <div class="form-group">
                        <label for="progress">Which progress</label>
                        <input type="file" class="form-control" name="image">
                        <br>
                        <img src="images/<?= $result['icon']?>" alt="" style="width: 100px; height: 100px;">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
</section>



<?php include '../layouts/footer.php';?>
</body>
</html>