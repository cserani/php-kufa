<?php
require '../database/connect.php';

$id = $_GET['id'];
$sql= "SELECT * FROM `about` WHERE `id`='$id'";
$query= mysqli_query($database,$sql);
$result= mysqli_fetch_assoc($query);

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php';?>
</head>
<body>
<?php include '../layouts/navbar.php';?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <form action="update.php" method="post" enctype="multipart/form-data">
                    <input type="text" value="<?=$id?>" name="id" hidden>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" id="name"
                               aria-describedby="emailHelp" placeholder="Enter your name"  class="form-control" name="name" value="<?= $result['name']?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Website</label>
                        <input type="text" class="form-control" id="website"
                               placeholder="Enter your website"  class="form-control" name="website" value="<?= $result['website']?>">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">About Me</label>
                        <textarea name="about_me" id="aboutMe" cols="30" rows="5" class="form-control" placeholder="Tell yourself"><?= $result['about_me']?></textarea>
                    </div>
                    <div class="form-group">
                        <label for="picture">Upload Image</label>
                        <input type="file"  class="form-control" name="image">
                        <img src="images/<?= $result['picture'] ?>" alt="" style="width: 80px; height: 80px;">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
</section>



<?php include '../layouts/footer.php';?>
</body>
</html>