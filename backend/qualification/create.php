<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php';?>
</head>
<body>
<?php include '../layouts/navbar.php';?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

<form action="store.php" method="post">
  <div class="form-group">
    <label for="title">Title (Ex:Diploma In computer Technology)</label>
    <input type="text" class="form-control" id="title"
     aria-describedby="emailHelp" placeholder="Enter your title"  name="title">
  </div>
  <div class="form-group">
    <label for="year">Which Year You passed</label>
    <input type="number" id="year"
     placeholder="Enter year"  class="form-control" name="year">
  </div>
    <div class="form-group">
    <label for="progress">Which progress</label>
    <input type="number" id="progress"
     placeholder="Enter progress"  class="form-control" name="progress">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
    
            </div>
        </div>
    </div>
</section>



<?php include '../layouts/footer.php';?>
</body>
</html>