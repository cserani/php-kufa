<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php'; ?>
</head>
<body>
<?php include '../layouts/navbar.php'; ?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <form action="store.php" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Title</label>
                        <input type="text" class="form-control" id="title"
                               aria-describedby="emailHelp" placeholder="Enter your title" class="form-control"
                               name="title">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Description</label>
                        <input type="text" class="form-control" id="description"
                               aria-describedby="emailHelp" placeholder="Enter your description" class="form-control"
                               name="description">
                    </div>
                    <div class="form-group">
                        <label for="facebook">facebook</label>
                        <input type="text" class="form-control" id="facebook"
                               placeholder="Enter your facebook" class="form-control" name="facebook">
                    </div>
                    <div class="form-group">
                        <label for="twitter">twitter</label>
                        <input type="text" class="form-control" id="twitter"
                               placeholder="Enter your facebook" class="form-control" name="twitter">
                    </div>
                    <div class="form-group">
                        <label for="linkedin">linkedin</label>
                        <input type="text" class="form-control" id="linkedin"
                               placeholder="Enter your linkedin" class="form-control" name="linkedin">
                    </div>
                    <div class="form-group">
                        <label for="pinterest">pinterest</label>
                        <input type="text" class="form-control" id="pinterest"
                               placeholder="Enter your pinterest" class="form-control" name="pinterest">
                    </div>
                    <div class="form-group">
                        <label for="picture">Upload Image</label>
                        <input type="file" class="form-control" name="image">
                    </div>

                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
</section>


<?php include '../layouts/footer.php'; ?>
</body>
</html>