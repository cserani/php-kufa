<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php';?>
</head>
<body>
<?php include '../layouts/navbar.php';?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

<form action="store.php" method="post" enctype="multipart/form-data">
  <div class="form-group">
    <label for="name">Name </label>
    <input type="text" class="form-control" id="name"
      placeholder="Enter your name"  name="name">
  </div>
  <div class="form-group">
    <label for="designation">Designation </label>
    <input type="text" class="form-control" id="designation"
      placeholder="Enter your designation"  name="designation">
  </div>
  <div class="form-group">
    <label for="des">Description</label>
    <input type="text" id="des"
     placeholder="Enter Description"  class="form-control" name="description">
  </div>
    <div class="form-group">
    <label for="image">Upload Picture</label>
    <input type="file" id="image" class="form-control" name="image">
  </div>
  <button type="submit" class="btn btn-primary">Submit</button>
</form>
    
            </div>
        </div>
    </div>
</section>



<?php include '../layouts/footer.php';?>
</body>
</html>