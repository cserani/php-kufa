<?php
session_start();
require '../database/connect.php';
// execute query
$sql = "SELECT * FROM `contact`";
$query= mysqli_query($database, $sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php';?>
</head>
<body>
<?php include '../layouts/navbar.php';?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="offset-2 col-md-8 mt-3 ">
            <a href="create.php" class="btn btn-success">+Add </a>
            <br>
            <br>
                <?php
                if (isset($_SESSION['delete'])){
                    ?>
                    <div  class="alert alert-danger">
                        <p><?=  $_SESSION['delete'];?></p>
                    </div>
                    <?php
                    unset($_SESSION['delete']);
                }
                ?>

                <table class="table table-striped">
                <thead class="table-dark">
                    <tr>
                    <th scope="col">#</th>
                    <th scope="col">Title</th>
                    <th scope="col">Address</th>
                    <th scope="col">Mobile</th>
                    <th scope="col">Action</th>
                    </tr>
                </thead>
                <tbody>
                <?php
                foreach($query as $key=>$value){
                ?>

                <tr>
                    <th scope="row"><?=++$key?></th>
                    <td><?= $value['title']?></td>
                    <td><?= $value['address']?></td>
                    <td><?= $value['email']?></td>
                    <td><a href="edit.php?id=<?= $value['id']?>">Edit</a>|| <a href="delete.php?id=<?= $value['id']?>">Delete</a></td>
                    </tr>


                    <?php
                }

                ?>

                </tbody>
                </table>

            </div>
        </div>
    </div>
</section>



<?php include '../layouts/footer.php';?>
</body>
</html>