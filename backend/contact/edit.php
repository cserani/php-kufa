<?php
require '../database/connect.php';

$id = $_GET['id'];
$sql= "SELECT * FROM `contact` WHERE `id`='$id'";
$query= mysqli_query($database,$sql);
$value= mysqli_fetch_assoc($query);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <?php include '../layouts/header.php';?>
</head>
<body>
<?php include '../layouts/navbar.php';?>
<section id="about">
    <div class="container">
        <div class="row">
            <div class="col-md-6">

                <form action="update.php" method="post">
                    <input type="text" value="<?=$id?>" name="id" hidden>
                    <div class="form-group">
                        <label for="title">Title </label>
                        <input type="text" class="form-control" id="title"
                        placeholder="Enter title"  name="title" value="<?=$value['title']?>">
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input type="text" id="address"
                        placeholder="Enter address"  class="form-control" name="address"  value="<?=$value['address']?>">
                    </div>
                    <div class="form-group">
                        <label for="Description">Description</label>
                        <input type="text" id="Description"
                        placeholder="Enter Description"  class="form-control" name="description" value="<?=$value['description']?>">
                    </div>

                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" id="email"
                        placeholder="Enter Email Address"  class="form-control" name="email"  value="<?=$value['email']?>">
                    </div>
                        <div class="form-group">
                        <label for="progress">Mobile</label>
                        <input type="number" id="mobile"
                        placeholder="Enter Mobile Number" class="form-control" name="mobile"  value="<?=$value['mobile']?>">
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </form>

            </div>
        </div>
    </div>
</section>



<?php include '../layouts/footer.php';?>
</body>
</html>