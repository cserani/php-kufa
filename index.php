<?php

require 'backend/database/connect.php';
//  About execute query
$aboutsql = "SELECT * FROM `about` LIMIT  1";
$aboutquery= mysqli_query($database, $aboutsql);
$aboutqueryresult= mysqli_fetch_assoc($aboutquery);

//  Banner execute query
$bannerSql = "SELECT * FROM `banner` LIMIT  1";
$bannerQuery= mysqli_query($database, $bannerSql);
$bannerresult= mysqli_fetch_assoc($bannerQuery);

//  Qualification execute query
$bannerSql = "SELECT * FROM `qualification`";
$bannerQuery= mysqli_query($database, $bannerSql);
//  services execute query
$servicesSql = "SELECT * FROM `services`";
$servicesQuery= mysqli_query($database, $servicesSql);

//  services execute query
$portfolioSql = "SELECT * FROM `portfolio`";
$portfolioQuery= mysqli_query($database, $portfolioSql);



//  counter execute query
$counterSql = "SELECT * FROM `counts`";
$counterQuery= mysqli_query($database, $counterSql);


//  Testimonial execute query
$testimonialSql = "SELECT * FROM `testimonial`";
$testimonialQuery= mysqli_query($database, $testimonialSql);

//  Testimonial execute query
$brandSql = "SELECT * FROM `brand`";
$brandQuery= mysqli_query($database, $brandSql);


$contactsql = "SELECT * FROM `contact` LIMIT  1";
$contactquery= mysqli_query($database, $contactsql);
$contactresult= mysqli_fetch_assoc($contactquery);


?>

<!-- header-start -->

 <?php  include 'includes/header.php'; ?>

<!-- header-end -->

<!-- main-area -->
<main>

    <!-- banner-area -->
    <?php  include 'includes/banner.php'?>
    <!-- banner-area-end -->

    <!-- about-area-->
    <?php  include 'includes/about.php'?>
    <!-- about-area-end -->

    <!--  Services-area include -->
    <?php  include 'includes/service.php'?>

    <!-- Services-area-end -->

    <!-- Portfolios-area -->
    <?php  include 'includes/portfolio.php'?>

    <!-- portfolio-area-end -->


    <!-- counter-area -->
    <?php  include 'includes/counter.php'?>
    <!-- counter-area-end -->

    <!-- testimonial-area -->
    <?php  include 'includes/testimonial.php'?>

    <!-- testimonial-area-end -->

    <!-- brand-area -->
    <?php  include 'includes/brand.php'?>
    <!-- brand-area-end -->

    <!-- contact-area -->
    <?php  include 'includes/contact.php'?>
    <!-- contact-area-end -->

</main>
<!-- main-area-end -->

<!-- footer -->
<?php  include 'includes/footer.php'?>
<!-- footer-end -->




